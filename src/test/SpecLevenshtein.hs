module SpecLevenshtein where

import Test.Hspec
import Test.QuickCheck (property, (===), PrintableString(..))

import Data.HashMap.Strict (empty)
import Data.Text (pack)
import Control.Monad.State.Strict (evalState)

import Velvet

specLevenshtein :: Spec
specLevenshtein = parallel $ do
    describe "levenshtein'" $
        it "gives the same result as levenshtein" $
            property $ \(x, y) -> let tx = pack $ getPrintableString x
                                      ty = pack $ getPrintableString y
                                      a  = levenshtein' tx ty
                                      b  = evalState (levenshtein tx ty) empty
                                   in a === b

    describe "levenshtein" $ do
        context "when its arguments are equal" $
            it "returns 0" $
                property $ \x -> let tx = pack . getPrintableString $ x
                                  in runCached (levenshtein tx tx) === 0

        it "is commutative" $
            property $ \(x, y) -> let tx = pack . getPrintableString $ x
                                      ty = pack . getPrintableString $ y
                                   in runCached (levenshtein tx ty) === runCached (levenshtein ty tx)

        it "can insert a character" . example $ 
            runCached (levenshtein (pack "kitten") (pack "kittens"))
                `shouldBe` 1

        it "can insert multiple characters" . example $ 
            runCached (levenshtein (pack "tea") (pack "treacle"))
                `shouldBe` 4

        it "can delete a character" . example $ 
            runCached (levenshtein (pack "deleted") (pack "delete"))
                `shouldBe` 1

        it "can delete multiple characters" . example $ 
            runCached (levenshtein (pack "multiple") (pack "mile"))
                `shouldBe` 4

        it "can substitute a character" . example $ 
            runCached (levenshtein (pack "treacle") (pack "treacly"))
                `shouldBe` 1

        it "can substitute multiple characters" . example $ 
            runCached (levenshtein (pack "a string") (pack "a WXriYZ"))
                `shouldBe` 4
