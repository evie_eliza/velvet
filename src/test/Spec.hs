import Test.Hspec

import SpecLevenshtein
import SpecDamerau

main :: IO ()
main = hspec $ sequence_
                 [ specLevenshtein
                 , specDamerau
                 ]
