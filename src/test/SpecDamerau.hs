module SpecDamerau where

import Test.Hspec
import Test.QuickCheck

import Data.HashMap.Strict (empty)
import Data.Text (pack)
import Control.Monad.State.Strict (evalState)

import Velvet

specDamerau :: Spec
specDamerau = parallel $ do
    describe "damerau'" $
        it "gives the same result as damerau" $
            verbose . property $ \(x, y) -> let tx = pack . take 10 $ getPrintableString x
                                                ty = pack . take 10 $ getPrintableString y
                                                a  = damerau' tx ty
                                                b  = evalState (damerau tx ty) empty
                                             in a === b

    describe "damerau" $ do
        context "when its arguments are equal" $
            it "returns 0" $
                verbose . property $ \x -> let tx = pack . take 10 . getPrintableString $ x
                                            in runCached (damerau tx tx) === 0

        it "is commutative" $
            verbose . property $ \(x, y) -> let tx = pack . take 10 . getPrintableString $ x
                                                ty = pack . take 10 . getPrintableString $ y
                                             in runCached (damerau tx ty) === runCached (damerau ty tx)

        it "can insert a character" . example $ 
            runCached (damerau (pack "kitten") (pack "kittens")) `shouldBe` 1

        it "can insert multiple characters" . example $ 
            runCached (damerau (pack "tea") (pack "treacle")) `shouldBe` 4

        it "can delete a character" . example $ 
            runCached (damerau (pack "deleted") (pack "delete")) `shouldBe` 1

        it "can delete multiple characters" . example $ 
            runCached (damerau (pack "multiple") (pack "mile")) `shouldBe` 4

        it "can substitute a character" . example $ 
            runCached (damerau (pack "treacle") (pack "treacly")) `shouldBe` 1

        it "can substitute multiple characters" . example $ 
            runCached (damerau (pack "a string") (pack "a STriNG")) `shouldBe` 4

        it "can swap characters" . example $
            runCached (damerau (pack "whomst") (pack "whomts")) `shouldBe` 1
