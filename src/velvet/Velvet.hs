{-# LANGUAGE FlexibleContexts #-}
module Velvet
    ( levenshtein
    , damerau
    , levenshtein'
    , damerau'
    , runCached
    , Cache
    ) where

import Prelude hiding (lookup, null, length, head, tail)

import Data.List (foldl1')
import Data.Text (Text, cons, uncons, null, length, head, tail)
import qualified Data.Text as T
import Data.HashMap.Strict (HashMap, lookup, empty, insert)
import Control.Monad.State.Strict

type Cache = HashMap (Text, Text) Int

runCached :: State Cache a -> a
runCached = flip evalState empty

-- | Levenshtein distance: the minimum number of operations required to
-- transform one string into another, where allowed operations are insertion of
-- a character ("tea" -> "teas"), deletion of a character ("teas" -> "tea"),
-- and substitution of one character to another ("tea" -> "tee").
levenshtein :: (MonadState Cache m) => Text -> Text -> m Int
levenshtein s t
    | null s = return $ length t
    | null t = return $ length s
    | s > t  = levenshtein t s -- reduces redundancy in the cache
    | otherwise = do
        cached <- gets $ lookup (s, t)
        case cached of
            Just x  -> return x
            Nothing -> let -- previous patterns guarantee s and t are non-empty
                           Just (s', ss) = uncons s
                           Just (t', ts) = uncons t
                        in if s' == t'
                           then do
                               result <- levenshtein ss ts
                               modify $ insert (s, t) result
                               return result
                           else do
                               result <- (1+) <$> foldl1' (liftM2 min)
                                        [ levenshtein s  ts -- deletion from t
                                        , levenshtein ss t  -- deletion from s
                                        , levenshtein ss ts -- substitution
                                        ]
                               modify $ insert (s, t) result
                               return result

    -- Enforce (s < t) so that (t, s) will be cached the same as (s, t).

-- | Run @levenshtein@, and immediately discard its cache when it completes.
-- Binding of @\x y -> evalState (levenshtein x y) empty@
levenshtein' :: Text -> Text -> Int
levenshtein' x y = evalState (levenshtein x y) empty

-- | Damerau-Levenshtein distance: the minimum number of operations required to
-- transform one string into another, where allowed operations are insertion of
-- a character ("tea" -> "teas"), deletion of a character ("teas" -> "tea"),
-- substitution of one character to another ("tea" -> "tee"), and transposition
-- of adjacent characters ("tea" -> "tae").
damerau :: (MonadState Cache m) => Text -> Text -> m Int
damerau s t
  | null s = return $ length t
  | null t = return $ length s
  | s < t  = damerau t s -- reduces redundancy in the cache
  | null (tail s) = if head s == head t
                       then return $ length t - 1
                       else return $ length t
  | null (tail t) = if head s == head t
                       then return $ length s - 1
                       else return $ length s
  | head s == head t = damerau (tail s) (tail t)
  | otherwise = do
      cached <- gets $ lookup (s, t)
      case cached of
        Just x  -> return x
        Nothing -> do
          let (s0, s1, ss) = unconsTwice s
          _ <- modify $ insert (s, t) (length t) -- upper bound, so swapping twice in a row resolves by cache instead of looping infinitely
          result <- (1+) <$> foldl1' (liftM2 min)
                           [ damerau (tail s) (tail t) -- substitute first char
                           , damerau (tail s) t -- delete first char
                           , damerau s (tail t) -- insert first char
                           , damerau (cons2 s1 s0 ss) t -- swap first two chars
                           ]
          _ <- modify $ insert (s, t) result
          return result
            where unconsTwice xs = let Just (x0, xs')  = uncons xs
                                       Just (x1, xs'') = uncons xs'
                                    in (x0, x1, xs'')
                  cons2 x y z = x `cons` (y `cons` z)


-- | Run @damerau@, and immediately discard its cache when it completes.
-- Binding of @\x y -> evalState (damerau x y) empty@
damerau' :: Text -> Text -> Int
damerau' x y = evalState (damerau x y) empty
