module Sorting where

import Control.Monad (when)
import Control.Monad.Extra (loopM)
import Data.Array.MArray
import Data.Foldable (traverse_)
import Data.Ord

insertionSortBy :: (Ix i, Enum i, MArray a e m) => (e -> e -> Ordering) -> a i e -> m ()
insertionSortBy cmp xs = do
  (start, end) <- getBounds xs
  let bubbleBackOne bubble = if bubble == start
                                then return $ Right ()
                                else do
                                  elem <- readArray xs bubble
                                  prev <- readArray xs (pred bubble)
                                  when (cmp elem prev == LT) $ do
                                    writeArray xs bubble prev
                                    writeArray xs (pred bubble) elem
                                  return $ Left (pred bubble)
  let bubbleBack ary i = loopM bubbleBackOne i
  traverse_ (bubbleBack xs) [start..end]

insertionSortOn :: (Ix i, Enum i, MArray a e m, Ord e') => (e -> e') -> a i e -> m ()
insertionSortOn = insertionSortBy . comparing

insertionSort :: (Ix i, Enum i, Ord e, MArray a e m) => a i e -> m ()
insertionSort = insertionSortOn id
