{-# LANGUAGE NamedFieldPuns #-}

import Prelude hiding (putStrLn, getLine)

import Control.Applicative (many)
import Control.Monad.Extra (loopM, ifM)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.State.Strict (StateT, evalStateT)
import Data.Foldable (traverse_)
import Data.HashMap.Strict (empty)
import Data.List (sortOn)
import Data.Text (Text, pack)
import Data.Text.IO (getLine, putStrLn)
import Options.Applicative hiding (empty)
import System.IO (isEOF)

import Velvet

data Config = Config { candidates :: [Text]
                     } deriving (Show)

(>-) :: (a0 -> a1 -> b) -> (b -> c) -> (a0 -> a1 -> c)
f >- g = \x y -> g (f x y)

invocation :: ParserInfo Config
invocation = (pure Config
    <*> (argument >- many) (maybeReader $ Just . pack)
          ( metavar "items"
         <> help "The list of items to be selected from by the user"
          ) <**> helper) `info` fullDesc

mainLoop :: [Text] -> StateT Cache IO (Either [Text] ())
mainLoop candidates = ifM (liftIO isEOF) (return $ Right ()) $ do
  line <- liftIO getLine
  costs <- traverse (damerau line) candidates
  let sortByCosts = map snd . sortOn fst . zip costs
  let sortedCandidates = sortByCosts candidates
  liftIO $ traverse_ putStrLn sortedCandidates
  return $ Left sortedCandidates

main :: IO ()
main = do
  Config { candidates } <- execParser invocation
  let runLoop = flip evalStateT empty . flip loopM candidates
  runLoop mainLoop
